<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('chats');
});

Route::resource('auth','AuthController');
Route::resource('chats','ChatsController');
Route::GET('admin_chats','ChatsController@editAdmin');
Route::GET('addChannel/{id}','ChatsController@addToChannel');
Route::GET('chats/send_message/{id}','ChatsController@sendMessage');
Route::GET('/logOut','AuthController@logOut');
