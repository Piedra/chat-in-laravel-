<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Twilio\Rest\Client;
require_once '../vendor/autoload.php';


class api extends Model
{
    public function createService(){
        // Find your Account Sid and Auth Token at twilio.com/console
        
        if($this->listServices() == null){
            $sid = env('TWILIO_ACCOUNT_SID');
            $token =  env('TWILIO_AUTH_TOKEN');
            $twilio = new Client($sid, $token);

            return $twilio->chat->v2->services
                                        ->create("Laravel_Api");

        }
        return $this->listServices();
    }

    public function listServices(){
        // Find your Account Sid and Auth Token at twilio.com/console
        $sid = env('TWILIO_ACCOUNT_SID');
        $token = env('TWILIO_AUTH_TOKEN');
        $twilio = new Client($sid, $token);

        return $twilio->chat->v2->services->read();
    }

}
