<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Api;
use Twilio\Rest\Client;
require_once '../vendor/autoload.php';


class ChatsController extends Controller
{
    protected $server;
    protected $client;
    private $service_sid = "IS2d7802b088da4818be6f79456335c796";


    public function __construct(Api $server) 
    {
        $this->middleware('checkRole')->except(['index','show','addToChannel','getMessages','sendMessage']);
        $this->server = $server;
        $this->client = new Client(env('TWILIO_ACCOUNT_SID'),env('TWILIO_AUTH_TOKEN'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->nickName){
            $request->session()->push('NickName', $request->nickName);
        }
        $names = $this->getNames(); 
        $myChannels = $this->getMyChannels(); 
        return view('Chats.index',compact('names','myChannels'));
    }


     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getNames(){
        $channels = $this->getChannels();
        $names = array();
        foreach ($channels as $channel) {
            $twilio = new Client($this->client->accountSid, $this->client->password);
            $channel = $twilio->chat->v2->services($channel->serviceSid)
                                        ->channels($channel->sid)
                                        ->fetch();

           array_push($names,$channel);
        }
        return $names;
   }

   public function getMyChannels(){
    $channels = $this->getChannels();
    $myChannels = array();
    foreach ($channels as $channel) {
        $twilio = new Client($this->client->accountSid, $this->client->password);
        $channel = $twilio->chat->v2->services($channel->serviceSid)
                                    ->channels($channel->sid)
                                    ->fetch();

       if($this->ifExist($channel->sid)){
        array_push($myChannels,$channel);
       }
    }
    return $myChannels;
}


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getChannels(){
         // Find your Account Sid and Auth Token at twilio.com/console
         
         $twilio = new Client($this->client->accountSid, $this->client->password);
         return $twilio->chat->v2->services($this->service_sid)
                                      ->channels
                                      ->read();
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('chats.create');
    }

    /**
     * Shows all the channels to be edit be the admin.
     *
     * @return \Illuminate\Http\Response
     */
    public function editAdmin()
    {   
        $channels = $this->getChannels();
        $names = array();
        foreach ($channels as $channel) {
            $twilio = new Client($this->client->accountSid, $this->client->password);
            $channel = $twilio->chat->v2->services($channel->serviceSid)
                                        ->channels($channel->sid)
                                        ->fetch();
           array_push($names,$channel);
        }
        return view('chats.admin',compact('names'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // Find your Account Sid and Auth Token at twilio.com/console
        $twilio = new Client($this->client->accountSid, $this->client->password);
        $channel = $twilio->chat->v2->services($this->service_sid)
                                    ->channels
                                    ->create(array("friendlyName" => $request->name));
        return redirect('chats');
    }

    /**
     * Loads all the messages of a channel, and retrieves the data in json format 
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getMessages($id)
    {
        $twilio = new Client($this->client->accountSid, $this->client->password);
        return $twilio->chat->v2->services($this->service_sid)
                                     ->channels($id)
                                     ->messages
                                     ->read();
    }


    /**
     * Gets all the members from a specific chat
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function listOfMembers($id)
    {
        $twilio = new Client($this->client->accountSid, $this->client->password);
        $members = $twilio->chat->v2->services($this->service_sid)
                                    ->channels($id)
                                    ->members
                                    ->read();

        return $members;
    }

    
    /**
     * Sees if the nickName Already exist in the chat
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ifExist($chaID)
    {
        $exist = false;
        $members = $this->listOfMembers($chaID);
        foreach ($members as $member) {
            $nick = $member->identity;
            if($nick == session()->get('NickName')[0])
            {
                $exist = true;
            };
        }
        return $exist;
    }

    /**
     * Sends a message
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sendMessage(Request $request,$id)
    {
        
        $twilio = new Client($this->client->accountSid, $this->client->password);
        $message = $twilio->chat->v2->services($this->service_sid)
                                    ->channels($id)
                                    ->messages
                                    ->create(array(
                                        "body" => $request->body_text,
                                        "from" => $request->NickName
                                ));
        return redirect('chats/'.$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $messages = $this->getMessages($id);
        $members = $this->listOfMembers($id);
        $twilio = new Client($this->client->accountSid, $this->client->password);
        $channel = $twilio->chat->v2->services($this->service_sid)
                            ->channels($id)
                            ->fetch();
        return view('chats.chat',compact('messages','members','channel'));
    }

    /**
     * Add the current user to a channel, and if he already exist,
     * Sends a error message.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addToChannel(Request $request,$id)
    {
       
        if($request->session()->get('NickName')==null){
            return redirect()->back()->with('alert', 'You have to log in first to see this chat');
        }
        if($this->ifExist($id))
        {
        $request->session()->forget('chatSID');
        $request->session()->push('chatSID', $id);
        return redirect()->back()->with('alert', 'You already belong to this chat');
        }
        
        $twilio = new Client($this->client->accountSid, $this->client->password);
        $member = $twilio->chat->v2->services($this->service_sid)
                                   ->channels($id)
                                   ->members
                                   ->create($request->session()->get('NickName'));
        $request->session()->forget('chatSID');
        $request->session()->push('chatSID', $id);
        return redirect()->back()->with('success', 'You are now member of the chat');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $twilio = new Client($this->client->accountSid, $this->client->password);
        $channel = $twilio->chat->v2->services($this->service_sid)
                                        ->channels($id)
                                        ->fetch();
            return view('chats.edit',compact('channel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $twilio = new Client($this->client->accountSid, $this->client->password);
        $channel = $twilio->chat->v2->services($this->service_sid)
                                    ->channels($id)
                                    ->update(array(
                                                 "friendlyName" => $request->name
                                             )
                                    );
        return redirect('/admin_chats');                                
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        // Find your Account Sid and Auth Token at twilio.com/console
        $twilio = new Client($this->client->accountSid, $this->client->password);
        $twilio->chat->v2->services($this->service_sid)
                         ->channels($id)
                         ->delete();

        return redirect('/admin_chats');
    }
}
