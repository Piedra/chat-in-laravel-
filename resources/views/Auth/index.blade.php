<?php $tittle='Auth/Login'?>

@extends('layouts.app')

@section('content')
    <!DOCTYPE html>
    <html lang="en">
    <head>
    </head>
    <body>
        <div class="container myContainer col-md-12">
                <h1 class="tittle">LazyChats</h1>
                <div class="row">
                    <div class="login col-md-6">
                        <form  method="GET" action="auth/">
                            @csrf
                            <div style="width: 100%;" class="card  fullpurple ">
                            <div class="card-body">
                            <h2 class="tittle">Admin</h2>
                                    <div class="col-auto">
                                            <label class="sr-only" for="inlineFormInputGroup">Username</label>
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text"><i class="fas fa-user purple"></i></div>
                                                </div>
                                                <input required name="user" type="text" class="form-control" id="user" placeholder="Username">
                                            </div>
                                    </div>
                                    <div class="col-auto">
                                        <label class="sr-only" for="inlineFormInputGroup">Password</label>
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text"><i class="fas fa-key purple"></i></div>
                                                </div>
                                                <input  required name="pass" type="password" class="form-control" id="pass" placeholder="Password">
                                            </div>
                                    </div>
                                        <div style="padding-right:15px;" class="text-right">
                                            <button style="width: 100%;"class="btn btn-success button">Go Chat</button>
                                        </div>
                            </div>
                            </div>
                        </form>
                    </div>
                    <div class="login col-md-6">
                        <form  method="GET" action="auth/">
                            @csrf
                            <div style="width: 100%;" class="card  fullpurple ">
                                <div class="card-body">
                                    <h2 class="tittle">Simple User</h2>
                                    <div class="col-auto">
                                            <label class="sr-only" for="inlineFormInputGroup">Nickname</label>
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text"><i class="fas fa-user purple"></i></div>
                                                </div>
                                                <input required name="user" type="text" class="form-control" id="user" placeholder="Nickname">
                                            </div>
                                    </div>
                                    <div class="col-auto">
                                        <label class="sr-only" for="inlineFormInputGroup">Password</label>
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text"><i class="fas fa-key purple"></i></div>
                                                </div>
                                                <input disabled name="pass" type="password" class="form-control" id="pass" placeholder="Password"
                                                helper="Simple Users dont need Password">
                                            </div>
                                    </div>
                                        <div style="padding-right:15px;" class="text-right">
                                            <button style="width: 100%;"class="btn btn-success button">Go Chat</button>
                                        </div>
                                </div>
                                </div>
                            </form>
                    </div>
                </div>
                @if (session('alert'))
                    <div style="width: 400px;height:70px;" class="card errorCard">
                        <div class="card-body">
                            <div class="alert alert-alert">
                                <p class="errorMessage">{{session('alert')}}</p>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
    </body>
    </html>
@endsection