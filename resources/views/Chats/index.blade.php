<?php $tittle='Chats'?>

@extends('layouts.app')

@section('content')
    <!DOCTYPE html>
    <html lang="en">
        <head>
        </head>
            <body>
                <div class="container col-md-12 myContainer row">
                    <div class="col-md-12 column">
                            @if (session('alert'))
                                    <div class="alert alert-alert">
                                        <p class="errorMessage">{{session('alert')}}</p>
                                    </div>
                            @endif
                            @if (session('success'))
                                    <div class="alert alert-success">
                                        <p class="successMessage">{{session('success')}}</p>
                                    </div>
                            @endif
                    </div>
                    <div class="col-md-6">
                        <h2 class="tittle">All the chats</h2>
                        @foreach ($names as $item)
                        <div class="column">
                            <div class="card mb-3">
                               <div class="card-body">
                                 <h5 class="card-title">{{$item->friendlyName}}</h5>
                                 <p class="card-text">Persons in this chat: {{$item->membersCount}}</p>
                                 <p class="card-text"><small class="text-muted">Last updated at {{$item->dateUpdated->format('Y-m-d')}}</small></p>
                                   <form action="addChannel/{{$item->sid}}" method="GET">
                                       <button style="width:100%" class="btn btn-success">Join!</button>
                                   </form>
                               </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="col-md-6">
                            <h2 class="tittle">All My chats</h2>
                            @if (session()->get('NickName')!=null)
                            @foreach ($myChannels as $item)
                            <div class="column">
                                <div class="card mb-3">
                                   <div class="card-body">
                                     <h5 class="card-title">{{$item->friendlyName}}</h5>
                                     <p class="card-text">Persons in this chat: {{$item->membersCount}}</p>
                                     <p class="card-text"><small class="text-muted">Last updated at {{$item->dateUpdated->format('Y-m-d')}}</small></p>
                                       <form action="chats/{{$item->sid}}" method="GET">
                                           <button style="width:100%" class="btn btn-info">Chat!</button>
                                       </form>
                                   </div>
                                </div>
                            </div>
                            @endforeach                       
                            @else
                            <div class="column">
                                    <div class="card mb-3">
                                       <div class="card-body">
                                         <h4 class=" text-center text-info">To see your chats, please log in</h4>
                                       </div>
                                    </div>
                                </div>                    
                            @endif
                    </div>
                </div>
            </body>
    </html>
@endsection