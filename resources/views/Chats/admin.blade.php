<?php $tittle='Admin/Chats';
    $count = 1;
?>

@extends('layouts.app')

@section('content')
    <!DOCTYPE html>
    <html lang="en">
        <head>
        </head>
            <body>
                <div class="container col-md-12">
                        <table class="table table-striped">
                                <thead>
                                  <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Type</th>
                                    <th scope="col">Members Count</th>
                                    <th scope="col"><a href="{{ url('/chats/create') }}" style="color:white;" class="btn btn-success"><i class="fas fa-plus"></i></a></th>
                                  </tr>
                                </thead>
                                <tbody>
                                    @foreach ($names as $item)      
                                  <tr>
                                    <th scope="row">{{$count}}</th>
                                    <td>{{$item->friendlyName}}</td>
                                    <td>{{$item->type}}</td>
                                    <td>{{$item->membersCount}}</td>
                                    <form method="GET" action="chats/{{$item->sid}}/edit">
                                        @csrf
                                        <td><button class="btn btn-warning"><i class="fas fa-edit"></i></button></td>
                                    </form>
                                    <form action="chats/{{$item->sid}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <td><button class="btn btn-danger"><i class="fas fa-minus"></i></button></td>
                                    </form>
                                  </tr>
                                  <?php $count++;?>
                                  @endforeach
                                </tbody>
                              </table>
                </div>
            </body>
    </html>
@endsection