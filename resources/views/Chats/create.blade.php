<?php $tittle='Admin/Chats'?>

@extends('layouts.app')

@section('content')
    <!DOCTYPE html>
    <html lang="en">
        <head>
        </head>
            <body>
              <div class="container col-md-12">
                  <br>
                <div class="card border-success col-md-6">
                    <div class="card-body">
                      <form action="/chats" method="POST">
                          @csrf
                          <p class="text">You need a friendly name for Your Chat for Example:</p>
                          <p class="text purple"> I Love Hugging Cactus</p>
                          <label class=""for="friendly_name">Friendly Name</label>
                          <input name="name"required  type="text" class="form-control" placeholder="Name">
                          <br>
                          <div class="text-right">
                            <button style=""class="btn btn-success button">Let's GO !</button>
                          </div>
                      </form>
                    </div>
                </div>
              </div>
            </body>
    </html>
@endsection