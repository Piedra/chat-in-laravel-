<?php $tittle='Admin/Chats'?>

@extends('layouts.app')

@section('content')
    <!DOCTYPE html>
    <html lang="en">
        <head>
        </head>
            <body>
              <div class="container myContainer col-md-12">
                   <div class="row">
                        <div class="col-md-2">
                            <div class="card">
                                <div style="width:100%;height:80px;" class="card-body text-center bg-secondary text-white">
                                    <div class="dropdown">
                                      <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Members
                                      </button>
                                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        @foreach ($members as $member)
                                            <p class="text-center">{{$member->identity}}</p>
                                        @endforeach
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="card">
                                <div style="width:100%;height:80px;" class="card-body text-center bg-info text-white">
                                  <p class="tittle3">{{$channel->friendlyName}}</p>
                                </div>
                            </div>
                            <div class="card">
                                    <div style="width:100%;" class="card-body">
                                        <table class="table">
                                            <tbody>
                                                <thead>
                                                 <tr>
                                                   <th scope="col">From: </th>
                                                   <th scope="col">Message</th>
                                                   <th scope="col">Last</th>
                                                 </tr>
                                                </thead>
                                              @foreach ($messages as $message)
                                                <tr>
                                                    <td><p class="messageText">{{$message->from}}</p></td>
                                                    <td><p>{{$message->body}}</p></td>
                                                    <td><p>{{$message->dateCreated->format('Y-m-d H:i:s')}}</p></td>
                                                </tr>
                                              @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                            <div class="card">
                                    <div style="width:100%;" class="card-body">
                                        <form action="send_message/{{$channel->sid}}" method="GET">
                                            <div style="justify-content:space-between" class="row">
                                                <div class="column">
                                                    <p>{{session()->get('NickName')[0] . ":"}}</p>
                                                </div>
                                                <div style="width:65%">
                                                    <input type="hidden" name="NickName" value="{{session()->get('NickName')[0]}}">
                                                    <input required type="text" class="form-control" name="body_text">
                                                </div>
                                                <br>
                                                <div style="width:20%">
                                                    <button style="width:100%" class="btn btn-success">Send !</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="card">
                                <form action="" method="GET">
                                <div style="width:100%;height:80px;" class="card-body text-center bg-secondary text-white">
                                        <button style="width:100%" class="btn btn-info" type="submit"><i class="fas fa-sync-alt"></i></button>
                                </div>
                            </form>
                            </div>
                        </div>

                   </div>
              </div>
            </body>
    </html>
@endsection