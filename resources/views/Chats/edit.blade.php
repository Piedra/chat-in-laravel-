<?php $tittle='Admin/Chats'?>

@extends('layouts.app')

@section('content')
    <!DOCTYPE html>
    <html lang="en">
        <head>
        </head>
            <body>
              <div class="container col-md-12">
                  <br>
                <div class="card border-warning col-md-6">
                    <div class="card-body">
                      <form action="/chats/{{$channel->sid}}" method="POST">
                          @csrf
                          @method('PATCH')
                          <p class="text">Hmm, Diddin't like that name:</p>
                          <p class="purple">{{$channel->friendlyName}}</p>
                          <p class="text">Well Let's Change It Then.</p>
                          <label class=""for="friendly_name">New Name</label>
                          <input name="name" required type="text" class="form-control" placeholder="Name">
                          <br>
                          <div class="text-right">
                            <button style=""class="btn btn-success button">Nice Name !</button>
                          </div>
                      </form>
                    </div>
                </div>
              </div>
            </body>
    </html>
@endsection